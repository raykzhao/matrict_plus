# MatRiCT+

This is the implementation source code for the paper:

Muhammed F. Esgin, Ron Steinfeld, & Raymond K. Zhao. (2021). MatRiCT+: More Efficient Post-Quantum Private Blockchain Payments. IACR Cryptology ePrint Archive [2021/545](https://ia.cr/2021/545).

The folders `n10`, `n20`, and `n50` are the implementations for anonymity levels 1/10, 1/20, and 1/50, respectively. 

In order to compile the source code, you need the [Keccak](https://github.com/XKCP/XKCP) library.

1. Run `make` to compile the source code.

2. Run `./ringct` to start the benchmark. The first 3 values are the runtime (in CPU cycles) of SamMat, Spend, and Verify, respectively. The last value is the correctness ("1" means the transaction can pass the verification, and "0" otherwise). The transaction generation time in the paper is the sum of SamMat and Spend. 
